7.x-1.0-alpha29, 2019-01-28
-----------------------------

7.x-1.0-alpha28, 2019-01-17
-----------------------------

7.x-1.0-alpha27, 2019-01-16
-----------------------------

7.x-1.0-alpha26, 2019-01-11
-----------------------------

7.x-1.0-alpha25, 2018-11-15
-----------------------------

7.x-1.0-alpha24, 2018-11-02
-----------------------------

7.x-1.0-alpha23, 2018-10-17
-----------------------------

7.x-1.0-alpha22, 2018-10-11
-----------------------------

7.x-1.0-alpha21, 2018-10-10
-----------------------------

7.x-1.0-alpha20, 2018-09-21
-----------------------------

7.x-1.0-alpha19, 2018-08-29
-----------------------------
- Preparing to tag 7.x-1.0-alpha17 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha16 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha15 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha14 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha13 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha12 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha11 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha10 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha9 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha8 for uaqs_social_media.
- Preparing to tag 7.x-1.0-alpha7 for uaqs_social_media.
- Copy the subtree from the ua_quickstart UADIGITAL-1166-2 branch.
7.x-1.0-alpha17, 2018-07-30
-----------------------------

7.x-1.0-alpha16, 2018-07-20
-----------------------------

7.x-1.0-alpha15, 2018-06-19
-----------------------------

7.x-1.0-alpha14, 2018-05-18
-----------------------------

7.x-1.0-alpha13, 2018-04-25
-----------------------------

7.x-1.0-alpha12, 2018-04-20
-----------------------------

7.x-1.0-alpha11, 2018-03-30
-----------------------------

7.x-1.0-alpha10, 2018-03-09
-----------------------------

7.x-1.0-alpha9, 2017-09-15
-----------------------------

7.x-1.0-alpha8, 2017-09-15
-----------------------------

7.x-1.0-alpha7, 2017-08-17
-----------------------------
- Copy the subtree from the ua_quickstart UADIGITAL-1166-2 branch.
